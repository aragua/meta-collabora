SUMMARY = "Multimedia image with weston"
LICENSE = "MIT"

IMAGE_FEATURES += " ssh-server-openssh package-management splash"
# IMAGE_FEATURES += "tools-debug dev-pkgs dbg-pkgs tools-sdk"

inherit core-image

set_hostname (){
     #!/bin/sh -e
     echo "Collabora" > ${IMAGE_ROOTFS}/etc/hostname;
}
ROOTFS_POSTPROCESS_COMMAND += "set_hostname;"

IMAGE_INSTALL_append_imx6dl-riotboard = "bootconf "

IMAGE_INSTALL += "kernel-modules linux-firmware usbmount"

IMAGE_INSTALL += "htop kbd kbd-keymaps kbd-locale-fr"

IMAGE_INSTALL += "netconfig"

# Weston packages
REQUIRED_DISTRO_FEATURES = "wayland"
IMAGE_INSTALL += "\
	weston \
	weston-init \
"

IMAGE_INSTALL += "gfiles evince gthumb"

# Multimedia
IMAGE_INSTALL += " \
	gstreamer1.0-meta-video \
	gstreamer1.0-meta-audio \
	gstreamer1.0-plugins-base \
	gstreamer1.0-plugins-good \
	gstreamer1.0-plugins-ugly \
	gstreamer1.0-plugins-bad \
	v4l-utils \
"

# web browser (Buggy at runtime)
# IMAGE_INSTALL += "epiphany"
# IMAGE_INSTALL += "gtkbrowser"

# Transmission
IMAGE_INSTALL += "transmission transmission-gui transmission-client transmission-web"

# Demo and examples
IMAGE_INSTALL += "\
	weston-examples \
	gtk+3-demo \
	clutter-1.0-examples \
	demo \
	wldemo \
"
