SUMMARY = "Weston based image"
LICENSE = "MIT"

REQUIRED_DISTRO_FEATURES = "wayland"

inherit core-image

set_hostname (){
     #!/bin/sh -e
     echo "Collabora" > ${IMAGE_ROOTFS}/etc/hostname;
}

ROOTFS_POSTPROCESS_COMMAND += "set_hostname;"

IMAGE_INSTALL_append_imx6dl-riotboard = " bootconf "

IMAGE_INSTALL += "kernel-modules linux-firmware usbmount"

IMAGE_INSTALL += "weston weston-init weston-examples "

IMAGE_INSTALL += "systemd-analyze"

IMAGE_INSTALL += "gfiles evince "
