SUMMARY = "Multimedia image with weston"
LICENSE = "MIT"

IMAGE_FEATURES += "ssh-server-openssh package-management"
IMAGE_FEATURES += "tools-debug dev-pkgs dbg-pkgs tools-sdk"

inherit core-image

IMAGE_INSTALL += "kernel-modules linux-firmware usbmount"

IMAGE_INSTALL += "htop kbd kbd-keymaps kbd-locale-fr"

IMAGE_INSTALL += "netconfig"

# Weston packages
REQUIRED_DISTRO_FEATURES = "wayland"
IMAGE_INSTALL += "\
	weston \
	weston-init \
	graphene \
	graphene-test \
	gtk+4 \
	gtk+4-demo \
"

