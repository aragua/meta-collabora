DESCRIPTION = "USB drive auto mount"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

SRC_URI = " \
	file://usbmount.rules \
	file://usbmount@.service \
	file://usbmount.sh \
"

REQUIRED_DISTRO_FEATURES = "systemd"

inherit allarch

S = "${WORKDIR}"

do_install () {
    install -d ${D}${sysconfdir}/udev/rules.d
    install -m 0755 ${WORKDIR}/usbmount.rules ${D}${sysconfdir}/udev/rules.d/
    install -d ${D}${systemd_system_unitdir}
    install -m 0755 ${WORKDIR}/usbmount@.service ${D}${systemd_system_unitdir}
    install -d ${D}${bindir}
    install -m 0755 ${WORKDIR}/usbmount.sh ${D}${bindir}
    install -d ${D}/media
}

FILES_${PN} = " \
	${sysconfdir}/udev/rules.d/ \
	${systemd_system_unitdir} \
	${bindir} \
	/media \
"

RDEPENDS_${PN} += "bash util-linux"