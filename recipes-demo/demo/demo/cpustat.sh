#!/bin/bash

echo "cpufreq info"
echo "Min     : $(cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq)"
echo "Current : $(cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq)"
echo "Max     : $(cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq)"

exit 0
