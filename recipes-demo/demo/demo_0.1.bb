SUMMARY = "Simple demo application"
SECTION = "examples"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
	file://cpustat.sh \
	file://egl-demo.sh \
	file://gst-demo1.sh \
	file://gst-demo2.sh \
"

inherit allarch

DEPENDS = "htop"
RDEPENDS_${PN} = "bash gstreamer1.0"

do_install() {
	     install -d ${D}${bindir}
	     install -m 0755 ${WORKDIR}/cpustat.sh ${D}${bindir}
	     install -m 0755 ${WORKDIR}/egl-demo.sh ${D}${bindir}
	     install -m 0755 ${WORKDIR}/gst-demo1.sh ${D}${bindir}
	     install -m 0755 ${WORKDIR}/gst-demo2.sh ${D}${bindir}
}

FILES_${PN} += "${bindir}/*"
