SUMMARY = "Simple wayland application"
SECTION = "examples"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://wldemo.c"

S = "${WORKDIR}"

DEPENDS = "wayland"

do_compile() {
	     ${CC} ${LDFLAGS} wldemo.c -lwayland-client -o wldemo
}

do_install() {
	     install -d ${D}${bindir}
	     install -m 0755 wldemo ${D}${bindir}
}
