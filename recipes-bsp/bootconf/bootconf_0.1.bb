DESCRIPTION = "Board boot configuration"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

SRC_URI = " \
	file://extlinux.conf \
"

inherit allarch

S = "${WORKDIR}"

do_install () {
    install -d ${D}/boot/extlinux/
    install -m 0755 ${WORKDIR}/extlinux.conf ${D}/boot/extlinux/
}

FILES_${PN} = " \
	/boot/extlinux \
"
