require transmission.inc

LICENSE = "GPLv2 & GPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=0dd9fcdc1416ff123c41c785192a1895"

SRC_URI += " file://0006-libsystemd.patch "

SRC_URI[md5sum] = "c92829294edfa391c046407eeb16358a"
SRC_URI[sha256sum] = "35442cc849f91f8df982c3d0d479d650c6ca19310a994eccdaa79a4af3916b7d"

USER_FOR_AUTH ?= "admin"
PASS_FOR_AUTH ?= "admin"

PACKAGECONFIG[systemd] = "--with-systemd,--without-systemd,systemd,"
