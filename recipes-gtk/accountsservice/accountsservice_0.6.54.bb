LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

SRC_URI = "https://github.com/freedesktop/accountsservice/archive/${PV}.tar.gz"
SRC_URI[md5sum] = "989f7826cee959181b008f910079cb92"
SRC_URI[sha256sum] = "31ab3fddfbc9a3ed0f71a184c005934ac88649c03fcd19ab17f29e0e6a903a65"

DEPENDS += "glib-2.0 polkit dbus glib-2.0-native"

inherit meson pkgconfig gobject-introspection

FILES_${PN} += " /lib/systemd/* \
		 /usr/share/* \
		"
