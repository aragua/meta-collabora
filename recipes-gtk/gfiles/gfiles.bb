SUMMARY = "Simple gtk file browser"
DESCRIPTION = "Simple file browser with a few dependencies"
LICENSE="MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=63e0f4c9b7c0cc28e4cd35adb08b0d8c"

SRCREV = "88ca9129dddd3ea975f1f6462281726262b8f607"
SRC_URI = "git://github.com/aragua/gfiles"

S = "${WORKDIR}/git"

DEPENDS = "gtk+3"

inherit meson


