SUMMARY = "Multi-platform toolkit for creating GUIs"
DESCRIPTION = "GTK+ is a multi-platform toolkit for creating graphical user interfaces. Offering a complete \
set of widgets, GTK+ is suitable for projects ranging from small one-off projects to complete application suites."
HOMEPAGE = "http://www.gtk.org"
BUGTRACKER = "https://bugzilla.gnome.org/"
SECTION = "libs"

DEPENDS = "glib-2.0 cairo pango atk jpeg libpng gdk-pixbuf \
           gdk-pixbuf-native gstreamer1.0 graphene libepoxy "

RDEPENDS_${PN} += "bash"

LICENSE = "LGPLv2 & LGPLv2+ & LGPLv2.1+"

inherit meson pkgconfig gobject-introspection

EXTRA_OECONF += "   "

MAJ_VER = "${@oe.utils.trim_version("${PV}", 2)}"

SRC_URI = "http://ftp.gnome.org/pub/gnome/sources/gtk+/${MAJ_VER}/gtk+-${PV}.tar.xz \
	file://toto.patch \
	file://changedepsname.patch \
"

do_configure_prepend () {
	rm -rf ${S}/subprojects
}

SRC_URI[md5sum] = "047f05058d3ad6a3bbfcb48d3167099e"
SRC_URI[sha256sum] = "a947caa5296610b0f1d7a03b58df34765c227c577c78e683e75eea3251a67035"

S = "${WORKDIR}/gtk+-${PV}"

LIC_FILES_CHKSUM = "file://COPYING;md5=5f30f0716dfdd0d91eb439ebec522ec2 \
                    file://gtk/gtk.h;endline=25;md5=1d8dc0fccdbfa26287a271dce88af737 \
                    file://gdk/gdk.h;endline=25;md5=c920ce39dc88c6f06d3e7c50e08086f2 \
                    file://tests/testgtk.c;endline=25;md5=cb732daee1d82af7a2bf953cf3cf26f1"

PACKAGECONFIG ??= "${@bb.utils.filter('DISTRO_FEATURES', 'wayland x11', d)} \
"

PACKAGECONFIG[x11] = "-Dx11-backend=true,-Dx11-backend=false,at-spi2-atk fontconfig libx11 libxext libxcursor libxi libxdamage libxrandr libxrender libxcomposite libxfixes"
PACKAGECONFIG[wayland] = "-Dwayland-backend=true,-Dwayland-backend=false,libxkbcommon wayland wayland-protocols virtual/mesa wayland-native"
PACKAGECONFIG[vulkan] = "-Dvulkan=yes,-Dvulkan=no,"
PACKAGECONFIG[cups] = "-Dprint-backends=cups,-Dprint-backends=none,cups"
PACKAGECONFIG[media] = "-Dmedia=all,-Dmedia=none,"

FILES_${PN} += " /usr/share/* "

PACKAGES =+ "${PN}-demo"

FILES_${PN}-demo = "${bindir}/gtk4-demo \
                    ${bindir}/gtk4-demo-application "
