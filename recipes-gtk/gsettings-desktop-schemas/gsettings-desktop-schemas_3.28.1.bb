SUMMARY = "collection of GSettings schemas"
LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=4fbd65380cdd255951079008b364516c"

#SRC_URI = "https://github.com/GNOME/gsettings-desktop-schemas/archive/${PV}.tar.gz"

SRC_URI[archive.md5sum] = "83bb19d025f126fae495ab43a2f26f40"
SRC_URI[archive.sha256sum] = "f88ea6849ffe897c51cfeca5e45c3890010c82c58be2aee18b01349648e5502f"

DEPENDS = "gtk+3 glib-2.0 intltool-native"

inherit gnome pkgconfig gettext python3native

EXTRA_OECONF_remove = "--disable-schemas-install"

FILES_${PN} += " \
	/usr/lib/girepository-1.0/GDesktopEnums-3.0.typelib \
	/usr/share/gir-1.0/GDesktopEnums-3.0.gir \
	"
