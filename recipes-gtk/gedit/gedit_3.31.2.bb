SUMMARY = "GNOME editor"
SECTION = "x11/gnome"
LICENSE = "GPLv2+"
PR = "r2"

DEPENDS = "gvfs enchant gconf gnome-doc-utils glib-2.0 gtk+3 \
           gtksourceview4 iso-codes intltool-native gnome-common-native \
           python3-six-native gsettings-desktop-schemas libpeas"

LIC_FILES_CHKSUM = "file://COPYING;md5=75859989545e37968a99b631ef42722e"

inherit distro_features_check gnome gettext python3native

SRC_URI = "${GNOME_MIRROR}/${GNOMEBN}/${@gnome_verdir("${PV}")}/${GNOMEBN}-${PV}.tar.xz;name=archive \
           file://0001-Remove-help-directory-from-build.patch \
           "
SRC_URI[archive.md5sum] = "98a07ce68c98454c26e434bbcd58f8ef"
SRC_URI[archive.sha256sum] = "d09b8ac0b5580429009ca1e55db0b417a241dc9a220902ca74c4ef470091b7fe"

EXTRA_OECONF = "--disable-scrollkeeper \
                --enable-gvfs-metadata \
		--disable-spell \
	"

LDFLAGS += "-lgmodule-2.0 "

FILES_${PN} += "${libdir}/gedit-2/plugin* ${datadir}/gedit-2"
FILES_${PN}-dbg += "${libdir}/gedit-2/plugin-loaders/.debug ${libdir}/gedit-2/plugins/.debug"
