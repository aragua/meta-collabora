SUMMARY = "A thin layer of graphic data types"
HOMEPAGE = "http://ebassi.github.io/graphene/"
BUGTRACKER = "https://github.com/ebassi/graphene/pulls"
SECTION = "libs"

LICENSE = "MIT"

inherit meson pkgconfig gobject-introspection

SRC_URI = " \
	https://github.com/ebassi/graphene/releases/download/${PV}/graphene-${PV}.tar.xz \
	file://fixgraphene.patch \
	"

SRC_URI[md5sum] = "87e023dcf748da9fb83bed4f18f6a8e6"
SRC_URI[sha256sum] = "b3fcf20996e57b1f4df3941caac10f143bb29890a42f7a65407cd19271fc89f7"

LIC_FILES_CHKSUM = "file://LICENSE;md5=a7d871d9e23c450c421a85bb2819f648"

PACKAGECONFIG ??= "gobject introspection gcc_vector neon"

PACKAGECONFIG[gobject] = "-Dgobject_types=true,-Dgobject_types=false,"
PACKAGECONFIG[introspection] = "-Dintrospection=true,-Dintrospection=false,"
PACKAGECONFIG[gcc_vector] = "-Dgcc_vector=true,-Dgcc_vector=false,"
PACKAGECONFIG[sse2] = "-Dsse2=true,-Dsse2=false,"
PACKAGECONFIG[neon] = "-Darm_neon=true,-Darm_neon=false,"

PACKAGES += " ${PN}-test "

FILES_${PN} += " /usr/lib/graphene-1.0/* "
FILES_${PN}-test = " /usr/share/installed-tests/* "

