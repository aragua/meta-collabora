SUMMARY = "Image viewer and browser"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=59530bdf33659b29e73d4adb9f9f6552"

SRC_URI[archive.md5sum] = "e7c518ebb9270b3adc5a39f19e424b20"
SRC_URI[archive.sha256sum] = "c9bf6bb7502659f5e990069527584bb96193f7f5f9492f0bf5f65828f5024b66"

DEPENDS = "gtk+3 gsettings-desktop-schemas intltool-native"

inherit gnome pkgconfig gettext

SRC_URI += " file://remove_help_from_subdirs.patch "

EXTRA_OECONF_remove = "--disable-schemas-install"
