SUMMARY = "Gnome plugin library"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=4b54a1fd55a448865a0b32d41598759d"

SRC_URI[archive.md5sum] = "a20dc55c3f88ad06da9491cfd7de7558"
SRC_URI[archive.sha256sum] = "5b2fc0f53962b25bca131a5ec0139e6fef8e254481b6e777975f7a1d2702a962"

DEPENDS = "gtk+3 glib-2.0 intltool-native gnome-common-native python3-six-native "

inherit gnome pkgconfig gettext python3native gobject-introspection

do_compile_prepend() {
    export GIR_EXTRA_LIBS_PATH="${B}/libpeas/.libs"
}

EXTRA_OECONF_remove = "--disable-schemas-install"

PACKAGES =+ " ${PN}-demo"

FILES_${PN}-demo = " /usr/lib/peas-demo/* "
