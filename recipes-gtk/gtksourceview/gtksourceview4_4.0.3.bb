SUMMARY = "Portable C library for multiline text editing"
HOMEPAGE = "http://projects.gnome.org/gtksourceview/"

LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=fbc093901857fcd118f065f900982c24"

DEPENDS = "gtk+3 libxml2 intltool-native gnome-common-native glib-2.0-native"

PNAME = "gtksourceview"

S = "${WORKDIR}/${PNAME}-${PV}"

inherit gnomebase lib_package gettext distro_features_check gtk-doc gobject-introspection

SRC_URI = "http://ftp.gnome.org/pub/gnome/sources/gtksourceview/4.0/${PNAME}-${PV}.tar.xz"
SRC_URI[md5sum] = "91e01df36e1476083f7b8a474f7d0c34"
SRC_URI[sha256sum] = "3d55a7b82841d96bf414ced2e1e912d4881c9ba2f7b74f0ff8aad636087f9d73"

FILES_${PN} += " ${datadir}/gtksourceview-4"
