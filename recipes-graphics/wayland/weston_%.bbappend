FILESEXTRAPATHS_prepend := "${THISDIR}/${BPN}:"

SRC_URI += " \
	file://collabora.png \
	file://weston.ini \
"

do_install_append () {
	install -d ${D}/usr/share/backgrounds/
	install -m 0644 ${WORKDIR}/collabora.png ${D}/usr/share/backgrounds/
	install -d ${D}${sysconfdir}/xdg/weston/
	install -m 0644 ${WORKDIR}/weston.ini ${D}${sysconfdir}/xdg/weston/
}

FILES_${PN} += " \
	${sysconfdir}/xdg/weston/ \
	/usr/share/backgrounds/ \
"

